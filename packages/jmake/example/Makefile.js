const { JMAKE_CONFIG } = require("../src");

// Will simply wait before resolving the promise
const delay = (ms) =>
    new Promise((resolve) => {
        setTimeout(() => {
            resolve();
        }, ms);
    });

exports[JMAKE_CONFIG] = {
    extends: ["./Makefile_parent.js"],
};

// Synchronous example
exports.hello = () => {
    console.log("Hello, World!");
};

// Basic promise example using async/await
exports.await = async () => {
    await delay(1000);
    console.log("1s has passed!");
};

// Thrown errors will work fine
exports.error = async () => {
    throw new Error("This is a demo error");
};

// Arguments handling
exports.args = async (...args) => {
    console.log(`${args.length} arguments were passed:`);
    console.log(args.map((a) => `  -- ${a}`).join("\n"));
};
