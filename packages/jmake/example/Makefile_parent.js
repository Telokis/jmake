exports.hello = () => {
    console.log("This command is overriden by Makefile.js!");
};

exports.new = () => {
    console.log("The new command is defined in the parent!");
};
