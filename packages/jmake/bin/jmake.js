#!/usr/bin/env node

const argv = require("minimist")(process.argv.slice(2));

require("../src/index")({
    ...argv,
    command: argv._[0] || "all",
    args: argv._.slice(1),
});
