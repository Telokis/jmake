# jmake

## Purpose

jmake is a simple tool meant to run custom JavaScript functions in an easy way.  
By simply providing a `Makefile.js` file, you will be able to do everything you want.

## Installation

Simply run `npm i jmake -g` to make the `jmake` command available globally.  
Alternatively, you can use [npx](https://medium.com/@maybekatz/introducing-npx-an-npm-package-runner-55f7d4bd282b) which is shipped with `npm`: `npx jmake`.  
This allows you to add `jmake` as a devDependency and to use `npx jmake` where you would have used `jmake` directly.

## Usage

The most basic usage is to have a file `Makefile.js` at the root of your project.  
This `Makefile.js` have to export commands that will be available to jmake.

```js
// Can be executed with `jmake hello`
exports.hello = () => {
    console.log("Hello, World!");
};
```

You can also return a promise if you need to do asynchronous stuff.

```js
// Can be executed with `jmake asynchronous`
exports.asynchronous = () =>
    new Promise(resolve => resolve("Hello, World!")).then(hello => console.log(hello));
```

Of course, it works with async/await as well.

```js
const delay = ms =>
    new Promise(resolve => {
        setTimeout(() => {
            resolve();
        }, ms);
    });

// Can be executed with `jmake await`
exports.await = async () => {
    await delay(1000);
    console.log("1s has passed!");
};
```

You can even get arguments from the command line.

```js
exports.args = (arg1, arg2) => {
    console.log("arg1 -->", arg1);
    console.log("arg2 -->", arg2);
};
// $ jmake args hello world
// > arg1 --> hello
// > arg2 --> world
```

You can see and try those examples using the demo Makefile `example/Makefile.js`.

## Recursion

jmake allows the user to reuse `Makefile.js` files.  
You can have two `Makefile.js` with the first extending the second. This means that every command
defined in the second will be available as if it were defined in the first.

To do so, the child `Makefile.js` must export a config object using the `JMAKE_CONFIG` symbol like so:

```js
const { JMAKE_CONFIG } = require("jmake");

exports[JMAKE_CONFIG] = {
    extends: ["./Makefile_parent.js"],
};
```

The paths for the extended Makefiles is relative to the Makefile extending it. It can be absolute.

### Command resolution

The resolution is a [depth-first search](https://en.wikipedia.org/wiki/Depth-first_search) meaning that
it goes as deep as possible before considering siblings. (Using `--help` will show you the priority order)

### Example

The [`example/Makefile.js`](example/Makefile.js) extends [`example/Makefile_parent.js`](example/Makefile_parent.js).

```
# Show all available commands when executing the child Makefile
$ jmake -f example/Makefile.js --help
info: Available commands (In priority order):
info:
info: /home/telokis/jmake/packages/jmake/example/Makefile.js:
info:  - hello
info:  - await
info:  - error
info:  - args
info:
info:   /home/telokis/jmake/packages/jmake/example/Makefile_parent.js:
info:    - hello
info:    - new
```

```
# The "hello" command is defined in both the child and parent but the child takes precedence.
$ jmake -f example/Makefile.js hello
Hello, World!
```

```
# The "new" command doesn't exist in the child but is available in the parent.
$ jmake -f example/Makefile.js new
The new command is defined in the parent!
```

```
# The parent Makefile can still be used without issue.
$ jmake -f example/Makefile_parent.js hello
This command is overriden by Makefile.js!
```

## Help

Not specifying any command will attempt to find a command named `all`.

```
$ jmake
> error: The command 'all' does not exist in your Makefile.
```

You can recursively list all available commands by passing `-h` of `--help` as the command name:

```
$ jmake -h
info: Available commands (In priority order):
info:
info: /home/telokis/jmake/packages/jmake/example/Makefile.js:
info:  - hello
info:  - await
info:  - error
info:  - args
info:
info:   /home/telokis/jmake/packages/jmake/example/Makefile_parent.js:
info:    - hello
info:    - new
```

## Configuration

If you don't like the default name, you can provide a custom name to replace `Makefile.js`.  
To do so, you just have to add a `jmake` entry to your `package.json`:

```json
{
    "jmake": {
        "file": "custom-makefile.js"
    }
}
```

> The `package.json` must be in the directory in which you run `jmake` or the script won't be able to find it.

> Directly specifying a string for `jmake` is now **deprecated**. Use an object with a `file` property instead.

The path is relative to your `package.json`.  
You can specify an absolute path.

### Using the command line

You can also override the file used with the `-f <file>` or `--file <file>` argument.  
This will take precedence over the entry within your `package.json` file.

```
$ jmake hello -f ./subdir/custom-makefile.js
$ jmake --file ./subdir/custom-makefile.js hello
```

The argument parsing uses [minimist](https://github.com/substack/minimist) meaning you need to use the
`--` delimiter if you want to pass named arguments to you command.  
For example, if you do:

```
$ jmake hello -- -u --my-arg hello.js
```

Your `hello` command will be called with `"-u"`, `"--my-arg"`, `"hello.js"`.

### Using as a library

You can use `jmake` as a library by `require`-ing it directly.

```js
const jmake = require('jmake');

await jmake({
    command: "hello",
    args: ["arg1", "arg2"],
    file: "./example/Makefile.js"
});
```

## License

[MIT License](LICENSE)
