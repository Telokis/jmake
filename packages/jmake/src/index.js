const path = require("path");
const fs = require("fs");
const winston = require("winston");

const JMAKE_CONFIG = Symbol("JMAKE_CONFIG");

function getConfig(makefile, makefilePath) {
    let config = {
        extends: [],
    };

    if (JMAKE_CONFIG in makefile) {
        config = {
            ...config,
            ...makefile[JMAKE_CONFIG],
        };

        config.extends = config.extends.map((filepath) =>
            path.resolve(path.dirname(makefilePath), filepath),
        );
    }

    return config;
}

function getCommands(makefile, makefilePath) {
    if (makefile === null) {
        try {
            // eslint-disable-next-line no-param-reassign
            makefile = require(makefilePath);
        } catch (e) {
            winston.warn(
                `Unable to require extended Makefile '${makefilePath}' to list commands. Skipping`,
            );

            return null;
        }
    }

    const commands = {
        file: makefilePath,
        commands: Object.keys(makefile)
            .filter((cmd) => typeof makefile[cmd] === "function")
            .map((cmd) => `${cmd}`),
        extended: [],
    };

    const config = getConfig(makefile, makefilePath);

    for (let i = 0; i < config.extends.length; ++i) {
        const extended = config.extends[i];
        const extendedCommands = getCommands(null, extended);

        if (extendedCommands !== null) {
            commands.extended.push(extendedCommands);
        }
    }

    return commands;
}

async function executeCommandInFile(makefilePath, command, args, exitIfErrorOnRequire = true) {
    // Retrieve Makefile
    let makefile;
    try {
        makefile = require(makefilePath);
    } catch (e) {
        if (exitIfErrorOnRequire) {
            winston.error(`Unable to require '${makefilePath}'.`);
            winston.error(`Error: ${e.message}.`);
            winston.error("Can't do anything. Exiting...");

            process.exit(1);
        } else {
            winston.warn(`Unable to require '${makefilePath}'.`);
            winston.warn(`Error: ${e.message}.`);
            winston.warn(`Skipping this Makefile.`);

            return false;
        }
    }

    const config = getConfig(makefile, makefilePath);

    // Do the processing
    if (command in makefile) {
        const handler = makefile[command];

        if (typeof handler !== "function") {
            winston.error("The command must be a function.");
            process.exit(4);
        }

        const errorHandler = (e) => {
            winston.error("==============================================================");
            winston.error(e.message);
            winston.error("==============================================================");
            winston.error("| ^^^^ Something went wrong while executing the command ^^^^ |");
            winston.error("==============================================================");
            winston.error(e);
            winston.error("==============================================================");
            process.exit(3);
        };

        try {
            const result = handler(...args);
            Promise.resolve(result).catch(errorHandler);
        } catch (e) {
            errorHandler(e);
        }

        return true;
    }

    for (let i = 0; i < config.extends.length; i++) {
        const extendedMakefilePath = config.extends[i];

        if (await executeCommandInFile(extendedMakefilePath, command, args, false)) {
            return true;
        }
    }

    return false;
}

async function jmakeHandler({ command, args, ...config }) {
    // Activate colorization for output
    winston.remove(winston.transports.Console);
    winston.add(winston.transports.Console, {
        colorize: true,
        level: "info",
        json: false,
        stringify: true,
    });

    // Setup variables to find the Makefile location
    let fileName = "Makefile.js";
    const cwd = process.cwd();
    const packageJsonPath = path.resolve(cwd, "package.json");

    if (fs.existsSync(packageJsonPath)) {
        try {
            const packageJson = require(packageJsonPath);

            if (typeof packageJson.jmake === "string") {
                winston.warn(
                    "DEPRECATED: The 'jmake' property in your package.json should be an object.",
                );
                winston.warn(
                    "            Using a string is deprecated and will be removed in a future release.",
                );
                winston.warn("            Use 'jmake.file' instead.");
                fileName = packageJson.jmake;
            } else if (typeof packageJson.jmake === "object" && packageJson.jmake !== null) {
                if (typeof packageJson.jmake.file === "string") {
                    fileName = packageJson.jmake.file;
                }
            }
        } catch (e) {
            winston.warn(`Unable to require '${packageJsonPath}'.`);
            winston.warn(`Defaulting fileName to ${fileName}.`);
            winston.warn(`Error: ${e.message}.`);
        }
    }

    if ("f" in config || "file" in config) {
        fileName = config.f || config.file;
    }

    const makefilePath = path.resolve(cwd, fileName);

    if ("h" in config || "help" in config) {
        winston.info("Available commands (In priority order):");
        const commands = getCommands(null, makefilePath);

        const printCommands = (cmds, depth = 0) => {
            const prefix = "  ".repeat(depth);

            winston.info();
            winston.info(`${prefix}${cmds.file}:`);

            for (let i = 0; i < cmds.commands.length; i++) {
                const cmd = cmds.commands[i];

                winston.info(`${prefix} - ${cmd}`);
            }

            for (let i = 0; i < cmds.extended.length; i++) {
                const extendedCommands = cmds.extended[i];

                printCommands(extendedCommands, depth + 1);
            }
        };

        printCommands(commands);

        process.exit(0);
    }

    if (!(await executeCommandInFile(makefilePath, command, args))) {
        winston.error(`The command '${command}' does not exist in your Makefile.`);
        process.exit(2);
    }
}

module.exports = jmakeHandler;
module.exports.JMAKE_CONFIG = JMAKE_CONFIG;
