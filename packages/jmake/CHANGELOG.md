All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.4.1] - 2022-03-12

### Changed

-   Jmake can now be used programmatically by requiring it.

## [2.3.0] - 2019-12-08

### Added

-   Added TypeScript definition file.
-   Added `JMAKE_CONFIG` symbol to specify custom configuration.
-   Added `extends` config. Allows a Makefile to inherit another one.

### Changed

-   Improved display of error message.
-   The `jmake` property inside the user's package.json should now be an object.
    Using a string is deprecated. Use `jmake.file` instead.
-   Updated the help message to handle `extends`.

## [2.2.0] - 2019-12-06

### Added

-   Added [minimist](https://github.com/substack/minimist) to better handle options.

### Changed

-   Fix: The `-f` option isn't passed as argument to the commands anymore.

## [2.1.0] - 2019-04-11

### Added

-   Option `-f` to override the path to the Makefile from the CLI. Will take
    precedence over the `jmake` entry in th `package.json` file.

### Changed

-   Added a proper error handler to catch errors when the command handler
    is a synchronous function.

[2.1.0]: https://gitlab.com/Telokis/jmake/-/tags/v2.1.0
[2.2.0]: https://gitlab.com/Telokis/jmake/-/tags/v2.2.0
[2.3.0]: https://gitlab.com/Telokis/jmake/-/tags/v2.3.0
[unreleased]: https://gitlab.com/Telokis/jmake/tree/master
