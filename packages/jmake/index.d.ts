/**
 * @desc Export a config object with this symbol to configure special behavior.
 */
declare const JMAKE_CONFIG: symbol;

export { JMAKE_CONFIG };

/**
 * @desc Configures how Jmake will behave
 */
export interface JmakeConfig {
    /**
     * @desc The command to run. It should be defined in the Makefile.
     * Defaults to "all".
     */
    command?: string;

    /**
     * @desc The arguments that should be passed to the jmake command handler.
     * By default, take all process.argv positional arguments.
     */
    args?: Array<string>;

    /**
     * @desc File to use as a Makefile. Defaults to "Makefile.js".
     * Can also be overriden from the package.json's "jmake.file" property.
     */
    file?: string;

    /**
     * @desc Displays all commands and quits.
     */
    help?: bool;
}

/**
 * @desc This function will call the appropriate command in the Makefile.
 * @param config Can be used to configure how jmake behaves.
 */
declare function jmake(config?: JmakeConfig): Promise<void>;

export = jmake;
