type CommandMakerType = (command: string, opts: string) => string;

/**
 * Options available when instanciating a JmakeSequelizeCli object.
 */
interface JmakeSequelizeCliOptions {
    /**
     * String appended to command names.
     */
    suffix?: string;

    /**
     * String prepended to command names.
     */
    prefix?: string;

    /**
     * Whether to generate commands using SUFFIX_MODE or ARGUMENT_MODE (default)
     */
    mode?: Symbol;

    /**
     * Small function generating the shell command used to execute sequelize cli commands.
     * Can be overriden by individual CommandSets.
     */
    commandMaker?: CommandMakerType;
}

interface JmakeSequelizeCliCommandSetOptions {
    /**
     * The name of this command set.
     * Used to generate the command name in SUFFIX_MODE.
     * The command argument is checked against it in ARGUMENT_MODE.
     */
    name: string;

    /**
     * The sequelize cli options used to access a specific database configuration.
     */
    cliArgs: string;

    /**
     * Small function generating the shell command used to execute sequelize cli commands.
     * Takes precedence over the one defined in JmakeSequelizeCliOptions.
     */
    commandMaker?: CommandMakerType;
}

/**
 * @desc Export a config object with this symbol to configure special behavior.
 */
declare class JmakeSequelizeCli {
    static ARGUMENT_MODE: Symbol;
    static SUFFIX_MODE: Symbol;

    /**
     * Creates a new JmakeSequelizeCli instance which can be used to register CommandSets.
     *
     * @param options The global options for this instance. They are merged with DEFAULT_OPTIONS.
     */
    constructor(options?: JmakeSequelizeCliOptions);

    /**
     * Registers a new CommandSet. The first one to be registered will be the default one.
     *
     * @param setOptions Configuration object for this CommandSet. Must at least contain `name`
     *                   and `cliArgs`.
     */
    addCommandSet(setOptions: JmakeSequelizeCliCommandSetOptions): void;

    /**
     * Effectively registers the commands for jmake to find them.
     *
     * @param exports The `module.exports` of your Makefile.
     */
    extendExports(exports: object): void;
}

export = JmakeSequelizeCli;
