const JmakeSequelizeCli = require("../src");

const jmakeSequelizeCli = new JmakeSequelizeCli({
    prefix: "toto-",
    suffix: "-db",
    mode: JmakeSequelizeCli.ARGUMENT_MODE,
    commandMaker: (command, opts) => `npx sequelize "${command}" ${opts}`,
});

jmakeSequelizeCli.addCommandSet({
    name: "main",
    cliArgs: '--config "" --seeders-path "" --models-path "" --migrations-path ""',
    commandMaker: (command, opts) => `npx sequelize "${command}" ${opts}`,
});

jmakeSequelizeCli.extendExports(module.exports);
