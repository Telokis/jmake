All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v2.4.0]

### Changed

-   Finished README.md

## [v2.4.0-alpha.2] - 2019-12-23

### Added

-   Added `index.d.ts` to `package.json` files.

## [v2.4.0-alpha.1] - 2019-12-23

### Added

-   README.md
-   Added a definition file `index.d.ts`
-   Added support for `SUFFIX_MODE`

### Changed

-   Fixed precedence for `commandMaker`

## [v2.4.0-alpha.0] - 2019-12-22

### Added

-   Added the JmakeSequelizeCli class

[v2.4.0]: https://gitlab.com/Telokis/jmake/-/tags/v2.4.0
[v2.4.0-alpha.2]: https://gitlab.com/Telokis/jmake/-/tags/v2.4.0-alpha.2
[v2.4.0-alpha.1]: https://gitlab.com/Telokis/jmake/-/tags/v2.4.0-alpha.1
[v2.4.0-alpha.0]: https://gitlab.com/Telokis/jmake/-/tags/v2.4.0-alpha.0
[unreleased]: https://gitlab.com/Telokis/jmake/tree/master
