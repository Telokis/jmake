const { execSync: execSyncBase } = require("child_process");
const cloneDeep = require("lodash.clonedeep");
const merge = require("lodash.merge");

const commandsImpl = require("./commands");

const COMMANDS_LIST = [
    "migrate",
    "seeds",
    "up-seeds",
    "down-seeds",
    "drop",
    "create",
    "fresh",
    "regenerate",
    "generate",
];

const ARGUMENT_MODE = Symbol("jmake-sequelize-cli ARGUMENT_MODE");
const SUFFIX_MODE = Symbol("jmake-sequelize-cli SUFFIX_MODE");

const DEFAULT_OPTIONS = {
    suffix: "-db",
    mode: ARGUMENT_MODE,
    commandMaker: (command, opts) => `npx sequelize "${command}" ${opts}`,
};

const execSync = (cmd, stdio = "inherit") => {
    console.log();
    console.log(`=========================================================================`);
    console.log(`execSync > '${cmd}'`);

    execSyncBase(cmd, {
        shell: true,
        stdio,
        env: { ...process.env, FORCE_COLOR: true },
    });

    console.log(`=========================================================================`);
};

class JmakeSequelizeCli {
    constructor(options = DEFAULT_OPTIONS) {
        this.options = merge({}, DEFAULT_OPTIONS, options);
        this.commandSets = {};
        this.defaultSet = null;
    }

    addCommandSet(setOptions) {
        const options = cloneDeep(setOptions);

        this.commandSets[options.name] = options;

        if (!this.defaultSet) {
            this.defaultSet = options;
        }
    }

    extendExports(exports) {
        if (this.options.mode === SUFFIX_MODE) {
            return this.extendSuffix(exports);
        }

        return this.extendArgument(exports);
    }

    extendSuffix(exports) {
        const commandSetsArr = Object.values(this.commandSets);

        for (let i = 0; i < commandSetsArr.length; i++) {
            const commandSet = commandSetsArr[i];

            for (let j = 0; j < COMMANDS_LIST.length; ++j) {
                const command = COMMANDS_LIST[j];
                const commandHandler = commandsImpl[command];
                const exportName = [
                    this.options.prefix || "",
                    command,
                    this.options.suffix || "",
                    `-${commandSet.name}`,
                ].join("");

                // eslint-disable-next-line no-param-reassign
                exports[exportName] = () => {
                    commandHandler(
                        execSync,
                        commandSet.cliArgs,
                        commandSet.commandMaker || this.options.commandMaker,
                    );
                };
            }
        }
    }

    extendArgument(exports) {
        for (let i = 0; i < COMMANDS_LIST.length; ++i) {
            const command = COMMANDS_LIST[i];
            const commandHandler = commandsImpl[command];
            const exportName = [this.options.prefix || "", command, this.options.suffix || ""].join(
                "",
            );

            // eslint-disable-next-line no-param-reassign
            exports[exportName] = name => {
                let set = this.defaultSet;

                if (name && name in this.commandSets) {
                    set = this.commandSets[name];
                }

                if (!set) {
                    console.error(`[jmake-sequelize-cli] The commandSet ${name} doesn't exist.`);
                    throw new Error(`[jmake-sequelize-cli] The commandSet ${name} doesn't exist.`);
                }

                commandHandler(
                    execSync,
                    set.cliArgs,
                    set.commandMaker || this.options.commandMaker,
                );
            };
        }
    }
}

JmakeSequelizeCli.ARGUMENT_MODE = ARGUMENT_MODE;
JmakeSequelizeCli.SUFFIX_MODE = SUFFIX_MODE;

module.exports = JmakeSequelizeCli;
