exports.migrate = (execSync, sequelizeOpts, commandMaker) => {
    console.log("[jmake-sequelize-cli] Executing migrations");
    execSync(commandMaker("db:migrate", sequelizeOpts));
};

exports.seeds = (execSync, sequelizeOpts, commandMaker) => {
    exports["down-seeds"](execSync, sequelizeOpts, commandMaker);
    exports["up-seeds"](execSync, sequelizeOpts, commandMaker);
};

exports["up-seeds"] = (execSync, sequelizeOpts, commandMaker) => {
    console.log("[jmake-sequelize-cli] Executing seeds");
    execSync(commandMaker("db:seed:all", sequelizeOpts));
};

exports["down-seeds"] = (execSync, sequelizeOpts, commandMaker) => {
    console.log("[jmake-sequelize-cli] Undoing seeds");
    execSync(commandMaker("db:seed:undo:all", sequelizeOpts));
};

exports.drop = (execSync, sequelizeOpts, commandMaker) => {
    console.log("[jmake-sequelize-cli] Dropping db");
    try {
        execSync(commandMaker("db:drop", sequelizeOpts), ["inherit", "inherit", "pipe"]);
    } catch (e) {
        if (!/exist/.test(e.message)) {
            throw e;
        } else {
            console.warn("[jmake-sequelize-cli] Database does not exist. Proceeding anyway.");
        }
    }
};

exports.create = (execSync, sequelizeOpts, commandMaker) => {
    console.log("[jmake-sequelize-cli] Creating db");
    try {
        execSync(commandMaker("db:create", sequelizeOpts), ["inherit", "inherit", "pipe"]);
    } catch (e) {
        if (!/exists/.test(e.message)) {
            throw e;
        } else {
            console.warn("[jmake-sequelize-cli] Database exists. Proceeding anyway.");
        }
    }
};

exports.fresh = (execSync, sequelizeOpts, commandMaker) => {
    exports.drop(execSync, sequelizeOpts, commandMaker);
    exports.create(execSync, sequelizeOpts, commandMaker);
    exports.migrate(execSync, sequelizeOpts, commandMaker);
};

exports.regenerate = (execSync, sequelizeOpts, commandMaker) => {
    exports.drop(execSync, sequelizeOpts, commandMaker);
    exports.generate(execSync, sequelizeOpts, commandMaker);
};

exports.generate = (execSync, sequelizeOpts, commandMaker) => {
    exports.create(execSync, sequelizeOpts, commandMaker);
    exports.migrate(execSync, sequelizeOpts, commandMaker);
    exports["up-seeds"](execSync, sequelizeOpts, commandMaker);
};
