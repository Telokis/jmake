# jmake-sequelize-cli

## Purpose

jmake-sequelize-cli provides a simple way to register [sequelize-cli] commands for [jmake].
Support for multiple databases with not much configuration needed.

## Installation

Simply run `npm i jmake-sequelize-cli` in the project where your `Makefile.js` is.

## Example

Here is a very basic example showing the capabilities of this module.

```js
// Makefile.js
const JmakeSequelizeCli = require("jmake-sequelize-cli");

const jmakeSequelizeCli = new JmakeSequelizeCli();

jmakeSequelizeCli.addCommandSet({
    name: "main",
    cliArgs:
        '--config "./sequelize-config.js" --seeders-path "./seeders/" --models-path "./models/" --migrations-path "./migrations/"',
});

jmakeSequelizeCli.extendExports(module.exports);
```

The command `jmake --help` will then show:

```
info: Available commands (In priority order):
info:
info: /path/to/Makefile.js:
info:  - migrate-db
info:  - seeds-db
info:  - up-seeds-db
info:  - down-seeds-db
info:  - drop-db
info:  - create-db
info:  - fresh-db
info:  - regenerate-db
info:  - generate-db
```

## Usage

To use this module, there are three steps:

-   Creating a `JmakeSequelizeCli` instance with its configuration.
-   Adding one or more CommandSets using `addCommandSet` with the proper configuration.
-   Using `extendExports` to expose the commands to [jmake].

---

```ts
constructor(options?: JmakeSequelizeCliOptions)
```

The constructor takes an optional `options` parameter defined as follows:

```ts
interface JmakeSequelizeCliOptions {
    /**
     * String appended to command names.
     */
    suffix?: string;

    /**
     * String prepended to command names.
     */
    prefix?: string;

    /**
     * Whether to generate commands using SUFFIX_MODE or ARGUMENT_MODE.
     */
    mode?: SUFFIX_MODE | ARGUMENT_MODE;

    /**
     * Small function generating the shell command used to execute sequelize cli commands.
     * Can be overriden by individual CommandSets.
     */
    commandMaker?: (command: string, opts: string) => string;
}
```

The object you pass as parameter will be merged with the default one:

```ts
const DEFAULT_OPTIONS = {
    suffix: "-db",
    mode: ARGUMENT_MODE,
    commandMaker: (command, opts) => `npx sequelize "${command}" ${opts}`,
};
```

### suffix

This string will be appended to the generated [jmake] command name.  
For example, setting it to `"-cmd"` will alter the commands like so:

-   `jmake migrate` --> `jmake migrate-cmd`
-   `jmake seeds` --> `jmake seeds-cmd`
-   `jmake up-seeds` --> `jmake up-seeds-cmd`
-   `jmake down-seeds` --> `jmake down-seeds-cmd`
-   `jmake drop` --> `jmake drop-cmd`
-   `jmake create` --> `jmake create-cmd`
-   `jmake fresh` --> `jmake fresh-cmd`
-   `jmake regenerate` --> `jmake regenerate-cmd`
-   `jmake generate` --> `jmake generate-cmd`

By default, it is set to `"-db"`. You can set it to `""` to not use a suffix.  
This can be used in combination with [`prefix`](#prefix).

### prefix

This string will be prepended to the generated [jmake] command name.  
For example, setting it to `"sequelize-"` will alter the commands like so:

-   `jmake migrate` --> `jmake sequelize-migrate`
-   `jmake seeds` --> `jmake sequelize-seeds`
-   `jmake up-seeds` --> `jmake sequelize-up-seeds`
-   `jmake down-seeds` --> `jmake sequelize-down-seeds`
-   `jmake drop` --> `jmake sequelize-drop`
-   `jmake create` --> `jmake sequelize-create`
-   `jmake fresh` --> `jmake sequelize-fresh`
-   `jmake regenerate` --> `jmake sequelize-regenerate`
-   `jmake generate` --> `jmake sequelize-generate`

By default, it is set to `""`.  
This can be used in combination with [`suffix`](#suffix).

### mode

This option is set using one of two `Symbol`s found as static members of `JmakeSequelizeCli`:

-   `JmakeSequelizeCli.ARGUMENT_MODE`
-   `JmakeSequelizeCli.SUFFIX_MODE`

#### ARGUMENT_MODE

This is the default mode.  
The module will only generate one [jmake] command for each cli [command](#commands) no matter how many CommandSets are registered.  
It will use the first command argument to determine which CommandSet to use for the command.  
The first registered CommandSet will be considered the default one and will be used if no argument is
passed when executing the command.

#### SUFFIX_MODE

The module will generate one [jmake] command for each cli [command](#commands) for each CommandSet that is registered.  
Each command name will have the CommandSet name appended to its name. (After the configured [suffix](#suffix), if present)  
This is not recommended as it will create a lot of [jmake] commands.

### commandMaker

This module works by calling [sequelize-cli] using [`execSync`](https://nodejs.org/api/child_process.html#child_process_child_process_execsync_command_options).  
Depending on your setup, the default `commandMaker` may not be suitable for you (if you're relying on [Babel](https://babeljs.io/), for example).  
If this is the case, you can specify this option and alter the behavior.

The value must be a function with the following signature:

```ts
(command: string, opts: string) => string;
```

where:

-   `command` is the [sequelize-cli] command that must be run (for example `"db:migrate"`).
-   `opts` is the `cliArgs` of the current CommandSet.

The default implementation is the following:

```js
(command, opts) => `npx sequelize "${command}" ${opts}`;
```

This option can also be set per CommandSet instead of globally.  
If both are specified, the CommandSet one takes precedence.

---

```ts
addCommandSet(setOptions: JmakeSequelizeCliCommandSetOptions)
```

This method is used to register a new CommandSet.  
A CommandSet typically represents a sequelize-managed database.
The `setOptions` is defined like follows:

```ts
interface JmakeSequelizeCliCommandSetOptions {
    /**
     * The name of this command set.
     * Used to generate the command name in SUFFIX_MODE.
     * The command argument is checked against it in ARGUMENT_MODE.
     */
    name: string;

    /**
     * The sequelize cli options used to access a specific database configuration.
     */
    cliArgs: string;

    /**
     * Small function generating the shell command used to execute sequelize cli commands.
     * Takes precedence over the one defined in JmakeSequelizeCliOptions.
     */
    commandMaker?: (command: string, opts: string) => string;
}
```

### name

Unique string identifying this CommandSet.  
See [mode](#mode) for details on the usage.

### cliArgs

String containing the command line options used by [sequelize-cli] to determine how to work.

### CommandSet commandMaker

Overrides the global `commandMaker` option.  
Look at [commandMaker](#commandmaker) for more details.

---

```ts
extendExports(exports: object)
```

This method will add the commands to the `exports` argument. Effectively altering it.  
This is typically called with `module.exports`:

```ts
jmakeSequelizeCli.extendExports(module.exports);
```

This will replace commands with the same name as the registered ones.  
Specify a [prefix](#prefix) or [suffix](#suffix) if your commands are being replaced.

## Commands

The module uses the following core commands. These commands are then prefixed and/or suffixed depending on the
configuration in order to generate the definitive jmake commands.

### migrate

Executes [sequelize-cli] `db:migrate` command.

### seeds

Executes [down-seeds](#down-seeds) and [up-seeds](#up-seeds).

### up-seeds

Executes [sequelize-cli] `db:seed:all` command.

### down-seeds

Executes [sequelize-cli] `db:seed:undo:all` command.

### drop

Executes [sequelize-cli] `db:drop` command with a special try/catch to avoid failing if the database doesn't exist.

### create

Executes [sequelize-cli] `db:create` command with a special try/catch to avoid failing if the database already exists.

### fresh

Executes [drop](#drop), [create](#create) and [migrate](#migrate).

### regenerate

Executes [drop](#drop) and [generate](#generate).

### generate

Executes [create](#create), [migrate](#migrate) and [up-seeds](#up-seeds).

## License

[MIT License](LICENSE)

[sequelize-cli]: https://github.com/sequelize/cli
[jmake]: https://www.npmjs.com/package/jmake
