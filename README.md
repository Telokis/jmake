# jmake

This project uses a [monorepo](https://github.com/lerna/lerna).

Information regarding the packages can be found in their respective directories:

-   [jmake](packages/jmake)
